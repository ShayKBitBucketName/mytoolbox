FROM alpine:3.5

RUN apk update \
    && apk add bash \
    && apk add bash-completion \
    && apk add curl \
    && apk add py-pip \
    && apk --update add redis \
    && apk --update add postgresql-client \
    && pip install awscli

ENTRYPOINT /bin/bash
